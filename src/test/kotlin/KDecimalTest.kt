import KDecimal.Companion.toKDecimal
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.math.BigDecimal.ZERO
import java.math.RoundingMode.HALF_UP

internal class KDecimalTest {

    @Test
    fun plus() {
        val bd1 = "-3.12345".toBigDecimal()
        val bd2 = "4.516457".toBigDecimal()

        val kDecimal1 = bd1.toKDecimal()
        val kDecimal2 = bd2.toKDecimal()
        val kDecimal3 = kDecimal1 + kDecimal2

        assertEquals((bd1 + bd2).toPlainString(), kDecimal3.toString())
    }

    @Test
    fun minus() {
        val bd1 = "-3.12345".toBigDecimal()
        val bd2 = "4.516457".toBigDecimal()

        val kDecimal1 = bd1.toKDecimal()
        val kDecimal2 = bd2.toKDecimal()
        val kDecimal3 = kDecimal1 - kDecimal2

        assertEquals((bd1 - bd2).toPlainString(), kDecimal3.toString())
    }

    @Test
    fun times() {
        val bd1 = "-3.12345".toBigDecimal()
        val bd2 = "4.516457".toBigDecimal()

        val kDecimal1 = bd1.toKDecimal()
        val kDecimal2 = bd2.toKDecimal()
        val kDecimal3 = kDecimal1 * kDecimal2

        assertEquals((bd1.multiply(bd2).setScale(6, HALF_UP)).toPlainString(), kDecimal3.toString())
    }

    @Test
    fun div() {
        val bd1 = "-3.12345".toBigDecimal()
        val bd2 = "4.516457".toBigDecimal()

        val kDecimal1 = bd1.toKDecimal()
        val kDecimal2 = bd2.toKDecimal()
        val kDecimal3 = kDecimal1 / kDecimal2

        assertEquals((bd1.divide(bd2, 6, HALF_UP)).toPlainString(), kDecimal3.toString())
    }

    @Test
    @Disabled("Needs enabling/rewriting once overflow detection has been written")
    fun overflow() {
        val initialStep = "10000000000000000"
        (0..15).forEach { scale ->
            val startStep = initialStep.dropLast(scale).toBigDecimal()

            val maxValue = "576460752303423487".toBigDecimal().movePointLeft(scale).toKDecimal()
            println("Checking overflow $maxValue for scale $scale")
            assertEquals(maxValue, findOverflow(startStep, ONE.setScale(scale).toKDecimal()))

            val minValue = "-576460752303423488".toBigDecimal().movePointLeft(scale).toKDecimal()
            println("Checking underflow $minValue for scale $scale")
            assertEquals(minValue, findOverflow(startStep, ONE.negate().setScale(scale).toKDecimal(), underflow = true))

            assertThrows<IllegalArgumentException> {
                println(("576460752303423488".toBigDecimal().movePointLeft(scale)).toKDecimal())
            }
        }
    }

    private tailrec fun findOverflow(step: BigDecimal, value: KDecimal, underflow: Boolean = false): KDecimal {
        var updated = value
        while (if (underflow) updated.toString().toBigDecimal() < ZERO else updated.toString().toBigDecimal() > ZERO) {
            if (underflow) updated -= step.toKDecimal() else updated += step.toKDecimal()
        }
        val prevUpdated = if (underflow) updated + step.toKDecimal() else updated - step.toKDecimal()

        return if (step == ONE && step.scale() == value.scale) prevUpdated
        else if (step == ONE) findDecimalOverflow("0.1".toBigDecimal(), prevUpdated, underflow)
        else findOverflow(step.toPlainString().dropLast(1).toBigDecimal(), prevUpdated, underflow)
    }

    private tailrec fun findDecimalOverflow(step: BigDecimal, value: KDecimal, underflow: Boolean): KDecimal {
        var updated = value
        while (if (underflow) updated.toString().toBigDecimal() < ZERO else updated.toString().toBigDecimal() > ZERO) {
            if (underflow) updated -= step.toKDecimal() else updated += step.toKDecimal()
        }
        val prevUpdated = if (underflow) updated + step.toKDecimal() else updated - step.toKDecimal()

        return if (step.scale() == value.scale) prevUpdated
        else findDecimalOverflow(step.movePointLeft(1), prevUpdated, underflow)
    }
}
