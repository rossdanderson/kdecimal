import org.decimal4j.api.DecimalArithmetic
import org.decimal4j.scale.Scales
import java.lang.Long.parseUnsignedLong
import java.lang.Long.toUnsignedString
import java.math.BigDecimal
import java.math.RoundingMode
import java.math.RoundingMode.HALF_UP

private const val scaleOffset = 59

inline class KDecimal(private val underlying: Long): Comparable<KDecimal> {

    val scale: Int
        get() {
            return underlying.and(scaleBitPattern).ushr(scaleOffset).toInt()
        }

    private val negative: Boolean
        get() = underlying.and(negativeBitPattern) == negativeBitPattern

    private val unscaled: Long
        get() = underlying.and(unscaledBitPattern).let { if (negative) it.or(scaleBitPattern) else it }

    operator fun div(other: KDecimal): KDecimal = div(other, HALF_UP)

    fun div(other: KDecimal, roundingMode: RoundingMode, desiredScale: Int = LARGEST_SCALE): KDecimal =
        apply(other, roundingMode, desiredScale) { thisUnscaled: Long, otherUnscaled: Long ->
            divide(thisUnscaled, otherUnscaled)
        }

    operator fun times(other: KDecimal): KDecimal = times(other, HALF_UP)

    fun times(other: KDecimal, roundingMode: RoundingMode, desiredScale: Int = LARGEST_SCALE): KDecimal =
        apply(other, roundingMode, desiredScale) { thisUnscaled: Long, otherUnscaled: Long ->
            multiply(thisUnscaled, otherUnscaled)
        }

    operator fun plus(other: KDecimal): KDecimal = plus(other, HALF_UP)

    fun plus(other: KDecimal, roundingMode: RoundingMode, desiredScale: Int = LARGEST_SCALE): KDecimal =
        apply(other, roundingMode, desiredScale) { thisUnscaled: Long, otherUnscaled: Long ->
            add(thisUnscaled, otherUnscaled)
        }

    operator fun minus(other: KDecimal): KDecimal = minus(other, HALF_UP)

    fun minus(other: KDecimal, roundingMode: RoundingMode, desiredScale: Int = LARGEST_SCALE): KDecimal =
        apply(other, roundingMode, desiredScale) { thisUnscaled: Long, otherUnscaled: Long ->
            subtract(thisUnscaled, otherUnscaled)
        }

    private inline fun apply(
        other: KDecimal,
        roundingMode: RoundingMode,
        desiredScale: Int,
        crossinline block: DecimalArithmetic.(thisUnscaled: Long, otherUnscaled: Long) -> Long
    ): KDecimal {
        check(desiredScale >= LARGEST_SCALE) { "Desired scale must be a positive value, -1 to default to the largest" }
        val thisScale = scale
        val otherScale = other.scale

        val effectiveScale = when (desiredScale) {
            LARGEST_SCALE -> when {
                thisScale >= otherScale -> thisScale
                else -> otherScale
            }
            else -> desiredScale
        }
        val arithmetic = Scales.getScaleMetrics(effectiveScale).getArithmetic(roundingMode)
        val thisUnscaled = arithmetic.rescale(thisScale, this.unscaled)
        val otherUnscaled = arithmetic.rescale(otherScale, other.unscaled)

        return KDecimal(effectiveScale, arithmetic.block(thisUnscaled, otherUnscaled))
    }

    private fun DecimalArithmetic.rescale(fromScale: Int, unscaled: Long): Long =
        if (fromScale == scale) unscaled else fromUnscaled(unscaled, fromScale)

    override fun toString(): String = Scales.getScaleMetrics(scale).toString(unscaled)

    override fun compareTo(other: KDecimal): Int =
        Scales.getScaleMetrics(scale).defaultArithmetic.compareToUnscaled(unscaled, other.unscaled, other.scale)

    fun same(other: KDecimal, ignoreScale: Boolean = false): Boolean {
        return if (ignoreScale) compareTo(other) == 0 else underlying == other.underlying
    }

    companion object {
        private val scaleBitPattern: Long = parseUnsignedLong("01111${"0".repeat(scaleOffset)}", 2) // 01111000...
        private val unscaledBitPattern: Long = parseUnsignedLong("10000${"1".repeat(scaleOffset)}", 2) // 10000111...
        private val negativeBitPattern: Long = parseUnsignedLong("10000${"0".repeat(scaleOffset)}", 2) // 10000000...

        const val LARGEST_SCALE: Int = -1

        fun String.toKDecimal(): KDecimal {
            val scale = indexOf('.').let { place -> if (place == -1) 0 else length - 1 - place }
            return KDecimal(
                scale,
                Scales.getScaleMetrics(scale).defaultArithmetic.parse(this)
            )
        }

        fun BigDecimal.toKDecimal(): KDecimal {
            val scale = scale()
            return KDecimal(
                scale,
                Scales.getScaleMetrics(scale).defaultArithmetic.fromBigDecimal(this)
            )
        }

        private operator fun invoke(scale: Int, unscaled: Long): KDecimal {
            require(scale <= 15) { "Only scales up to 15 decimal places are supported" }
            return KDecimal(scale.toLong().shl(scaleOffset).or(unscaled.and(unscaledBitPattern)))
        }

        private fun Long.printBits() {
            println(toUnsignedString(this, 2).padStart(64, '0'))
        }
    }
}
