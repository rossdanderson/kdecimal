# KDecimal

Makes use of [decimal4j's](https://github.com/tools4j/decimal4j) zero-garbage operations and Kotlin's inline class 
feature to provide fixed scale KDecimal types that in certain situations be used in place of `BigDecimal` where garbage
collection is a concern, or `double` where precision is a concern.

All values are stored as a single primitive long with 1 bit for the sign, 4 bits for the scale, and 59 bits for the 
unscaled value.

By default, we use default rounding (`HALF_UP`), and the largest of the input parameter's scales for scale conversions, 
but these can each can be overridden by using the non-operator version of the functions.

## Example
```kotlin
println(("-3.12345".toKDecimal() + "900.32454".toKDecimal() * "2".toKDecimal() / "3.12".toKDecimal()).toString())
```
prints:
```text
574.00767
```
## Supported values

| Scale | Max Value           | Min Value            |
|-------|---------------------|----------------------|
|     0 | 576460752303423487  | -576460752303423488  |
|     1 | 57646075230342348.7 | -57646075230342348.8 |
|     2 | 5764607523034234.87 | -5764607523034234.88 |
|     3 | 576460752303423.487 | -576460752303423.488 |
|     4 | 57646075230342.3487 | -57646075230342.3488 |
|     5 | 5764607523034.23487 | -5764607523034.23488 |
|     6 | 576460752303.423487 | -576460752303.423488 |
|     7 | 57646075230.3423487 | -57646075230.3423488 |
|     8 | 5764607523.03423487 | -5764607523.03423488 |
|     9 | 576460752.303423487 | -576460752.303423488 |
|    10 | 57646075.2303423487 | -57646075.2303423488 |
|    11 | 5764607.52303423487 | -5764607.52303423488 |
|    12 | 576460.752303423487 | -576460.752303423488 |
|    13 | 57646.0752303423487 | -57646.0752303423488 |
|    14 | 5764.60752303423487 | -5764.60752303423488 |
|    15 | 576.460752303423487 | -576.460752303423488 |

## TODO

* JMH Benchmarks
* Detect overflow
* Input validation
